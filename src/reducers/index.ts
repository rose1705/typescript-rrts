import { combineReducers } from "redux";
import { todosReducer } from "./todos";
import { Todo } from "../actions";

// will have a property in store of todos: [Todo, Todo, Todo, etc]

export interface StoreState {
  todos: Todo[];
}

export const reducers = combineReducers<StoreState>({
  todos: todosReducer
});
